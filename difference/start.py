import os
import json
import time
import sys

from panda import Panda

import logger, unique

#pylint: disable-msg=too-many-arguments
class CANFinder():
    def __init__(self):
        print("Trying to connect to Panda over USB...")
        
        try:
            self.panda = Panda()
        except AssertionError:
            print("USB connection failed. Trying WiFi...")

        try:
            self.panda = Panda("WIFI")
        except:
            print("WiFi connection timed out. Please make sure your Panda is connected and try again.")
            sys.exit(0)

        directory = './output/'
        if not os.path.exists(directory):
            os.makedirs(directory)

    def run(self):
        newBackground = input("Make a new background file? (y/N): ")
        if (newBackground == "y"):
            if os.path.exists("./output/background.csv"):
                os.remove("./output/background.csv")

            print("Running background scanner... press any key to exit")
            time.sleep(5)
            logger.can_logger("./output/background.csv", self.panda)
        
        print("Running interesting scanner for the first time... press any key to exit")
        time.sleep(5)
        logger.can_logger("./output/interesting-1.csv", self.panda)

        input("Press enter to continue...")
        print("Running interesting scanner for the second time... press any key to exit")
        time.sleep(5)
        logger.can_logger("./output/interesting-2.csv", self.panda)

        print("=========== DIFFERENCE #1 ===========")
        unique.PrintUnique("./output/interesting-1.csv", "./output/background.csv")
        input("Press enter to continue...")
        print("\n")

        print("=========== DIFFERENCE #2 ===========")
        unique.PrintUnique("./output/interesting-2.csv", "./output/background.csv")
 
if __name__ == "__main__":
    can = CANFinder()
    while True:
        can.run()
        input("Press enter to continue...")